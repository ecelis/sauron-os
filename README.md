Sauron-OS
=========

Operating system for citizen's public services towers, based on
GNU/Linux.

Sauron-OS is a set of scripts, third party software and configuration
files designed to be deployed over an existing istallation of any of the
supported base GNU/Linux distributions.

It is designed to provide an operating systems for ValkEye city emergency
towers system running on bare iron with nodes spread in a wide area.

Originally it ran on top of Elastix on x86_64 hardware, then in Fedora
for ARM.

For developers it uses a mix of Vagrant with a VirtualBox provider, and
Ansible, both for development and production provisioning.


Install
-------

CentOS 6 is supported on the x86_64 platform. For ARM Fedora is the only
succesfully tested distribution on Cubietech boards. Read GNU/Linux
distribution specific install guidelines in corresponding
INSTALL.CentOS or INSTALL.Fedora files.

Please note that the server part is only supported on Elastix at this
time.


Sauron-OS is sponsored by
[ValkTechnologies](http://valktechnologies.com/)