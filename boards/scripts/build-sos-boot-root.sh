#!/bin/bash
#
#
#
WORK_DIR=
ROOTFS_DIR=
UBOOT_DIR=
TARCH=arm
TOOLCHAIN=arm-linux-gnueabihf-
BOARD=cubietruck
MAKE_JOBS=-j4

mkdir $UBOOT_DIR
mkdir $ROOTFS_DIR

#U-BOOT XC
cd $WORK_DIR/u-boot-sunxi
make distclean CROSS_COMPILE=$TOOLCHAIN
make $BOARD CROSS_COMPILE=$TOOLCHAIN

#SUNXI-TOOLS C
cd $WORK_DIR/suxi-tools
make clean
make $MAKE_JOBS

#SUNXI-KEERNEL ARM XC
cd $WORK_DIR/linux-sunxi
# Copy .config
make clean ARCH=$TARCH CROSS_COMPILE=$TOOLCHAIN
# make menuconfig
make -j4 ARCH=$TARCH CROSS_COMPILE=$TOOLCHAIN uImage modules
make ARCH=$TARCH CROSS_COMPILE=$TOOLCHAIN
make ARCH=$TARCH CROSS_COMPILE=$TOOLCHAIN INSTALL_MOD_PATH=$ROOTFS_DIR/usr modules_install

# SD
# dd if=spl/sunxi-spl.bin of=${card} bs=1024 seek=8
#dd if=u-boot.img of=${card} bs=1024 seek=40
#dd if=u-boot-sunxi-with-spl.bin of=${card} bs=1024 seek=8
## If using u-boot v2013.07 or earlier then the procedure is slightly different
# dd if=spl/sunxi-spl.bin of=${card} bs=1024 seek=8
# dd if=u-boot.bin of=${card} bs=1024 seek=32
dd if=/dev/zero of=$SDCARD bs=1M count=1
dd if=$WORK_DIR/u-boot/u-boot-sunxi-spl.bn of=$CARD bs=1024 seek=8
